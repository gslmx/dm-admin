import { CSSOptions } from 'vite';
import autoprefixer from 'autoprefixer';


 const postcssOptions: CSSOptions['postcss'] = {
  plugins: [
    autoprefixer()



  ]
}

export default postcssOptions;
