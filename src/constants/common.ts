export const COMMON_STATUS = [
  {
    label: '正常',
    value: '0',
    theme: 'success',
  },
  {
    label: '停用',
    value: '1',
    theme: 'danger',
  },
];
