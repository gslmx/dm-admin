import type { TRouterInfo } from '@/types/interface';
import { store } from '@store';

interface TabsRouterState {
  /** tabs 路由列表 */
  tabRouterList: TRouterInfo[];
  isRefreshing: boolean;
}

const homeRoute: Array<TRouterInfo> = [
  {
    path: '/dashboard/base',
    routeIdx: 0,
    title: '仪表盘',
    name: 'DashboardBase',
    isHome: true,
  },
];

export const useTabsRouterStore = defineStore(
  'tabs-router',
  () => {
    const tabsRouteState = reactive<TabsRouterState>({
      tabRouterList: homeRoute,
      isRefreshing: false,
    });

    const ignoreCacheRoutes = ['login', 'notFound'];
    const ignoreRoutes = ['login', 'notFound'];

    /** tabs 路由列表 */
    const tabRouters = computed(() => tabsRouteState.tabRouterList);
    /** 刷否刷新 */
    const refreshing = computed(() => tabsRouteState.isRefreshing);

    /** 处理刷新 */
    const toggleTabRouterAlive = (routeIdx: number) => {
      tabsRouteState.isRefreshing = !tabsRouteState.isRefreshing;
      tabsRouteState.tabRouterList[routeIdx].isAlive = !tabsRouteState.tabRouterList[routeIdx].isAlive;
    };

    /** 处理新增 */
    const appendTabRouterList = (newRoute: TRouterInfo) => {
      // 不添加路由
      if (ignoreRoutes.includes(newRoute.name as string)) {
        return;
      }
      // 不要将判断条件newRoute.meta.keepAlive !== false修改为newRoute.meta.keepAlive，starter默认开启保活，所以meta.keepAlive未定义时也需要进行保活，只有显式说明false才禁用保活。
      const needAlive = !ignoreCacheRoutes.includes(newRoute.name as string) && newRoute.meta?.keepAlive !== false;
      if (!tabRouters.value.find((route: TRouterInfo) => route.path === newRoute.path)) {
        // eslint-disable-next-line no-param-reassign
        tabsRouteState.tabRouterList = [...tabsRouteState.tabRouterList.concat({ ...newRoute, isAlive: needAlive })];
      }
    };

    /** 处理关闭当前 */
    const subtractCurrentTabRouter = (newRoute: TRouterInfo) => {
      const { routeIdx } = newRoute;
      tabsRouteState.tabRouterList = tabsRouteState.tabRouterList
        .slice(0, routeIdx)
        .concat(tabsRouteState.tabRouterList.slice(1 + (routeIdx as number)));
    };

    /** 处理关闭右侧 */
    const subtractTabRouterBehind = (newRoute: TRouterInfo) => {
      const { routeIdx } = newRoute;
      const homeIdx: number = tabRouters.value.findIndex((route: TRouterInfo) => route.isHome);
      let tabRouterList: Array<TRouterInfo> = tabsRouteState.tabRouterList.slice(0, 1 + (routeIdx as number));
      if ((routeIdx as number) < homeIdx) {
        tabRouterList = tabRouterList.concat(tabRouterList);
      }
      tabsRouteState.tabRouterList = tabRouterList;
    };

    /** 处理关闭左侧 */
    const subtractTabRouterAhead = (newRoute: TRouterInfo) => {
      const { routeIdx } = newRoute;
      const homeIdx: number = tabRouters.value.findIndex((route: TRouterInfo) => route.isHome);
      let tabRouterList: Array<TRouterInfo> = tabsRouteState.tabRouterList.slice(routeIdx);
      if ((routeIdx as number) > homeIdx) {
        tabRouterList = homeRoute.concat(tabRouterList);
      }
      tabsRouteState.tabRouterList = tabRouterList;
    };

    /**  处理关闭其他 */
    const subtractTabRouterOther = (newRoute: TRouterInfo) => {
      const { routeIdx } = newRoute;
      const homeIdx: number = tabRouters.value.findIndex((route: TRouterInfo) => route.isHome);
      tabsRouteState.tabRouterList =
        routeIdx === homeIdx ? homeRoute : homeRoute.concat([tabsRouteState.tabRouterList?.[routeIdx as number]]);
    };

    /** 删除 */
    const removeTabRouterList = () => {
      tabsRouteState.tabRouterList = [];
    };

    /** 初始化 */
    const initTabRouterList = (newRoutes: TRouterInfo[]) => {
      newRoutes?.forEach((route: TRouterInfo) => appendTabRouterList(route));
    };

    return {
      tabsRouteState,
      toggleTabRouterAlive,
      appendTabRouterList,
      subtractCurrentTabRouter,
      subtractTabRouterBehind,
      subtractTabRouterAhead,
      subtractTabRouterOther,
      removeTabRouterList,
      initTabRouterList,
      tabRouters,
      refreshing,
    };
  },
  // {
  //   persist: [
  //     {
  //       storage: window.sessionStorage,
  //       paths: ['tabsRouter'],
  //     },
  //   ],
  // },
);

export function getTabsRouterStore() {
  return useTabsRouterStore(store);
}
