import { createPinia } from 'pinia';
import { createPersistedState } from 'pinia-plugin-persistedstate';

const store = createPinia();
store.use(createPersistedState());

export { store };
export default store;

export * from './account';
export * from './notification';
export * from './setting';
export * from './tabs-router';
