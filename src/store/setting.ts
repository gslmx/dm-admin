export interface State {
  settingVisible: boolean;
  tabNavVisible: boolean;
  breadcrumbVisible: boolean;
  footerbVisible: boolean;
}

export const useSettingStore = defineStore(
  'setting',
  () => {
    const settingState = reactive<State>({
      settingVisible: false,
      tabNavVisible: false,
      breadcrumbVisible: false,
      footerbVisible: true,
    });

    const updateState = (payload: Partial<State>) => {
      for (let key in payload) {
        if (key in settingState) {
          const value = payload[key as keyof State];
          if (value) {
            toRef(settingState, key as keyof State).value = value;
          }
        }
      }
    };

    return {
      settingState,
      updateState,
    };
  },
  {
    persist: [
      {
        storage: window.sessionStorage,
        paths: ['settingState'],
      },
    ],
  },
);
