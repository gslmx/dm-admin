export default {
  //
  header: {
    user: '用户中心',
    signOut: '退出登录',
    signOutMsgTitle: '提示',
    signOutMsgContent: '你确定要退出吗？',
  },

  // 通知中心
  notice: {
    title: '通知中心',
    clear: '清空',
    setRead: '设为已读',
    allRead: '全部已读',
    emptyNotice: '暂无通知',
    viewAll: '查看全部',
  },
};
