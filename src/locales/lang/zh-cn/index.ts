import zhCn from 'element-plus/es/locale/lang/zh-cn.mjs';
import zhCNConfig from 'tdesign-vue-next/es/locale/zh_CN';
import dayZhCn from 'dayjs/locale/zh-cn';

import { merge } from 'lodash-es';

import layout from './layout';
import common from './common';

export default {
  lang: '简体中文',

  layout,
  common,
  componentsLocale: merge({}, zhCNConfig, {
    // custom properties
  }),
  dayLocale: dayZhCn,
};
