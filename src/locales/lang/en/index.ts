import en from 'element-plus/es/locale/lang/en.mjs';
import enUSConfig from 'tdesign-vue-next/es/locale/en_US';
import dayEn from 'dayjs/locale/en';

import { merge } from 'lodash-es';

import layout from './layout';
import common from './common';

export default {
  lang: 'English',

  layout,
  common,
  componentsLocale: merge({}, enUSConfig, {
    // custom properties
  }),
  dayLocale: dayEn,
};
