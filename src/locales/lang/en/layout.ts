export default {
  //
  header: {
    user: 'Profile',
    signOut: 'Sign Out',
    signOutMsgTitle: 'prompt',
    signOutMsgContent: 'are you sure to sign out?',
  },
  // 通知中心
  notice: {
    title: 'Notification Center',
    clear: 'Clear',
    setRead: 'Set Read',
    allRead: 'All Read',
    emptyNotice: 'No Notice',
    viewAll: 'View All',
  },
};
