import type { FallbackLocale } from 'vue-i18n';
import { createI18n } from 'vue-i18n';
import { useSessionStorage } from '@vueuse/core';
import { forIn } from 'lodash-es';

export const defaultLanguage = 'zh-cn';
// 导入语言文件
const langModules = import.meta.glob('./lang/*/index.ts', { eager: true });
// 语言列表
const langList: Array<{ name: string; key: string }> = [];
// 生成语言模块列表
const generateLangMessages = () => {
  const messages: { [key: string]: any } = {};

  forIn(langModules, (value, key) => {
    const langKey = key.replace('./lang/', '').replace('/index.ts', '');
    // @ts-ignore
    const messagesDefault = value.default;
    // @ts-ignore
    // console.log('lang', langKey, value?.default);
    langList.push({
      name: messagesDefault.lang,
      key: langKey,
    });
    // @ts-ignore
    messages[langKey] = messagesDefault;
  });

  return messages;
};
// LocalStorage key
const localeKey = 'lang';

/** 回退策略 */
const fallbackLocale: FallbackLocale = {
  zh: [defaultLanguage],
  'en-us': ['en', defaultLanguage],
};

const i18n = createI18n({
  // 使用 Composition API 模式，则需要将其设置为false
  legacy: false,
  // 全局注入 $t 函数
  globalInjection: true,
  // set locale
  locale: useSessionStorage(localeKey, navigator.languages[0].toLowerCase() || defaultLanguage).value,
  // set fallback locale
  fallbackLocale: fallbackLocale,
  //
  formatFallbackMessages: true,
  // set locale messages
  messages: generateLangMessages(),
  // 防止使用v-html=“$t(“”)”的xss警告
  warnHtmlMessage: false,
  //
  silentTranslationWarn: true,
  missingWarn: false,
  silentFallbackWarn: true,
  fallbackWarn: false,
});

export { i18n, langList, localeKey };

export default i18n;
