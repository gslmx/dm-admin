import { App } from 'vue';
import Index from './src/Index.vue';

export default {
  // 导出的组件名字
  install(app: App) {
    app.component('SvgIcon', Index);
  },
};
