// 字典
export interface SysDict {
  /** 字典主键 */
  dictId?: string;
  /** 字典名称 */
  dictName: string;
  /** 字典类型 */
  dictType: string;
  /** 状态 0：有效，1：无效 */
  status: number;
  /** 创建时间 */
  createTime?: string;
  /** 更新时间 */
  updateTime?: string;
  /** 创建人 */
  createBy?: string;
  /** 更新人 */
  updateBy?: string;
  /** 备注 */
  remarks?: string;
}

export interface SysDictData {
  /** 字典数据主键 */
  dictDataId?: string;
  /** 字典标签 */
  dictLabel: string;
  /** 字典键值 */
  dictValue: string;
  /** 字典类型 */
  dictType: string;
  /** 字典排序 */
  dictSort: number;
  /** 回显样式 */
  listClass?: string;
  /** 系统默认 */
  isDefault: 'Y' | 'N';
  /** 状态 0：有效，1：无效 */
  status: number;
  /** 创建时间 */
  createTime?: string;
  /** 更新时间 */
  updateTime?: string;
  /** 创建人 */
  createBy?: string;
  /** 更新人 */
  updateBy?: string;
}

// 字典
