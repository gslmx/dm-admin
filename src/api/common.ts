import { MenuRoute } from '@/types/interface';

export {};

const menuitems: MenuRoute[] = [
  {
    title: '仪表盘',
    path: '/dashboard/base',
    meta: {
      title: '仪表盘',
    },
  },
  {
    title: '系统管理',
    path: '/sys',
    meta: {
      title: '系统管理',
    },
    children: [
      {
        title: '用户管理',
        path: '/sys/user',
        meta: {
          title: '用户管理',
        },
      },
      {
        title: '角色管理',
        path: '/sys/role',
        meta: {
          title: '角色管理',
        },
      },
      {
        title: '菜单管理',
        path: '/sys/menu',
        meta: {
          title: '菜单管理',
        },
      },
      {
        title: '部门管理',
        path: '/sys/dept',
        meta: {
          title: '部门管理',
        },
      },
      {
        title: '字典管理',
        path: '/sys/dict',
        meta: {
          title: '字典管理',
        },
      },
    ],
  },
];

/**
 * 获取用户信息
 * @returns 用户信息
 */
export const getUserInfo = () =>
  new Promise<any>((resolve) => {
    setTimeout(() => {
      resolve({
        userInfo: {
          /** 用户Id */
          userId: 'assss111',
          /** 用户名 */
          username: 'admin',
          /** 昵称 */
          nickname: '管理员',
          /** 头像 */
          avatar: 'https://tdesign.gtimg.com/site/avatar.jpg',
        },
        menus: menuitems,
        permissions: ['*'],
      });
    }, 3000);
  });

/** 登录 */
export const login = (_loginInfo: any) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        code: 200,
        message: '登录成功',
        data: {
          token: '1234567890',
        },
      });
    }, 1000);
  });
