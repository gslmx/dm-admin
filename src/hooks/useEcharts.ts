import echarts from '@/utils/echarts';

export type EChartsCoreOption = echarts.EChartsCoreOption;

/**
 * useEcharts
 * @param elRef div ref
 * @param options 参数
 * @returns charts
 */
const useEcharts = (elRef: Ref<HTMLDivElement>, options: EChartsCoreOption) => {
  const charts = shallowRef<echarts.ECharts>();

  const initCharts = () => {
    charts.value = echarts.init(elRef.value);
    setOptions(options);
  };
  const setOptions = (options: EChartsCoreOption) => {
    charts.value && charts.value.setOption(options);
  };
  const echartsResize = () => {
    charts.value && charts.value.resize();
  };
  onMounted(() => {
    window.addEventListener('resize', echartsResize);
  });
  // 防止 echarts 页面 keepAlive 时，还在继续监听页面
  onDeactivated(() => {
    window.removeEventListener('resize', echartsResize);
  });

  onBeforeUnmount(() => {
    window.removeEventListener('resize', echartsResize);
  });
  return {
    initCharts,
    setOptions,
    echartsResize,
  };
};
export { useEcharts };
