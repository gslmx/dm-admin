import 'vue-router';

declare module 'vue-router' {
  interface RouteMeta {
    // 路由标题
    title?: Recordable<any>;
    // 是否需要登录
    requiresAuth?: boolean;
  }
}

export {};
