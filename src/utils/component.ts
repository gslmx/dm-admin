import type { App, DefineComponent, Plugin } from 'vue';

type SFCWithInstall<T> = T & Plugin;
export const withInstall = <T extends Component | DefineComponent>(comp: T): SFCWithInstall<T> => {
  (comp as SFCWithInstall<T>).install = (app: App) => {
    // 注册组件
    app.component((comp as any).name, comp);
  };
  return comp as SFCWithInstall<T>;
};
