import type { Emitter, EventType } from 'mitt';
import mitt from 'mitt';

/** miit 事件类型 */
export interface Events extends Record<EventType, unknown> {
  // 通知弹窗
  noticeVisible: boolean;

  // 待确定
  openPanel: string;
  tagViewsChange: string;
  tagViewsShowModel: string;
  logoChange: boolean;

  changLayoutRoute: string;
  imageInfo: {
    img: HTMLImageElement;
    height: number;
    width: number;
    x: number;
    y: number;
  };
}

const emitter: Emitter<Events> = mitt<Events>();

export default emitter;
