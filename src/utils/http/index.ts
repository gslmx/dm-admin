/** http请求 模块 */
import type { useFetch } from '@vueuse/core';

interface MyFetchOption {
  baseUrl?: string;
}

class MyFetch {
  private readonly baseUrl?: string;
  private readonly instanceFetch?: typeof useFetch;

  public constructor({ baseUrl }: MyFetchOption) {
    this.baseUrl = baseUrl;
  }

  public request() {
    console.log('request', this.baseUrl);

    return this.instanceFetch;
  }
}

export {};
export const httpRequest = new MyFetch({ baseUrl: '' }).request();
