import { createApp } from 'vue';
import router from './router';
import i18n from './locales';
import store from './store';

import SvgIcon from '@/components/SvgIcon';
import 'virtual:svg-icons-register';

import TDesign from 'tdesign-vue-next';

import App from './App.vue';

import 'animate.css/animate.min.css'; // /引入动画
// 引入组件库的少量全局样式变量
import 'tdesign-vue-next/es/style/index.css';

import './assets/style/index.less';

// import { addPreventDefault } from '@/utils/preventDefault';
// addPreventDefault(); // 阻止默认事件

let app = createApp(App);

app.use(router);
app.use(i18n);
app.use(store);
app.use(TDesign);
app.use(SvgIcon);

app.mount('#app');
