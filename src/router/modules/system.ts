import type { RouteRecordRaw } from 'vue-router';

const sysPage: RouteRecordRaw[] = [
  {
    path: '/sys/user',
    name: 'sysUser',
    meta: {
      title: {
        zh_CN: '用户管理',
        en_US: 'user mgt',
      },
    },
    component: () => import('@/pages/sys/user/index.vue'),
  },
  {
    path: '/sys/dept',
    name: 'sysDept',
    meta: {
      title: {
        zh_CN: '部门管理',
        en_US: 'dept mgt',
      },
    },
    component: () => import('@/pages/sys/dept/index.vue'),
  },
  {
    path: '/sys/role',
    name: 'sysRole',
    meta: {
      title: {
        zh_CN: '角色管理',
        en_US: 'role mgt',
      },
    },
    component: () => import('@/pages/sys/role/index.vue'),
  },
  {
    path: '/sys/menu',
    name: 'sysMenu',
    meta: {
      title: {
        zh_CN: '菜单管理',
        en_US: 'menu mgt',
      },
    },
    component: () => import('@/pages/sys/menu/index.vue'),
  },
  {
    path: '/sys/dict',
    name: 'sysDict',
    meta: {
      title: {
        zh_CN: '字典管理',
        en_US: 'dict mgt',
      },
    },
    component: () => import('@/pages/sys/dict/index.vue'),
  },
];

export default sysPage;
