import type { RouteRecordRaw } from 'vue-router';

const commonPage: RouteRecordRaw[] = [
  {
    path: '/sys/user/profile',
    name: 'UserProfile',
    component: () => import('@/pages/sys/user/Profile.vue'),
    meta: { title: { zh_CN: '个人中心', en_US: 'User Center' } },
  },
  {
    path: '/sys/common/notice',
    name: 'UserNotice',
    component: () => import('@/pages/sys/common/Notice.vue'),
    meta: { title: { zh_CN: '通知中心', en_US: 'Notification Center' } },
  },
];

export default commonPage;
