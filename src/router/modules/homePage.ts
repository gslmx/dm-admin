import type { RouteRecordRaw } from 'vue-router';

const dashboardPage: RouteRecordRaw[] = [
  {
    path: '/dashboard/base',
    name: 'DashboardBase',
    meta: {
      title: {
        zh_CN: '仪表盘',
        en_US: 'Overview',
      },
    },
    component: () => import('@/pages/dashboard/base/index.vue'),
  },
];

export default dashboardPage;
