import type { RouteRecordRaw } from 'vue-router';

export const Layout = () => import('@/layouts/index.vue');

export const defaultRouterList: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: {
        zh_CN: '登录',
        en_US: 'login',
      },
    },
    component: () => import('@/pages/login/index.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/notFound',
  },
];

export const IndexPage: RouteRecordRaw = {
  path: '/',
  name: 'index',
  redirect: '/dashboard/base',
  meta: {
    title: {
      zh_CN: '首页',
      en_US: 'index',
    },
  },
  component: Layout,
  children: [],
};

export const ErrorPage: RouteRecordRaw[] = [
  {
    path: '/notFound',
    name: 'notFound',
    meta: {
      title: {
        zh_CN: '资源不存在',
        en_US: 'not found',
      },
    },
    component: () => import('@/pages/error/notFound.vue'),
  },
];
