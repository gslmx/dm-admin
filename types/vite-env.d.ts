/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue';
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

interface ImportMetaEnv {
  /** 开发端口 */
  readonly VITE_PORT: number;
  /** 项目路径 */
  readonly VITE_PUBLIC_PATH: string;
  /** 路由类型 */
  readonly VITE_ROUTER_HISTORY: string;
  /** 开发环境是否开启请求代理 */
  readonly VITE_IS_REQUEST_PROXY: boolean;
  /** 开发环境请求代理地址 */
  readonly VITE_API_URL_PREFIX: string;
  /** 开发环境请求代理目标地址 */
  readonly VITE_API_URL: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
